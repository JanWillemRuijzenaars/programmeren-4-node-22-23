const express = require('express');
const router = express.Router();
const authController = require('../controllers/auth.controller');

// router.get('/', authController.getAll);
// router.get('/:id', authController.getOne);
// router.post('/', authController.create);
// router.put('/:id', authController.update);
// router.delete('/:id', authController.delete);

module.exports = router;